<%--
  Created by IntelliJ IDEA.
  User: suzan
  Date: 2019-02-11
  Time: 20:12
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>My Login Page</title>
    <style>

        body {
            font-size: x-large;
            align-self: center;
        }

        button {
            size:A3;
        }
    </style>
</head>
<body>
<div class="container-fluid">
    <div class="card" align="center">
        <h1>SIGN IN</h1>
        <form action="login">
            <!--<form method="post">-->
            <div class="form-group">
                <label for="emails">Email: </label>
                <input type="email" class="form-control" id="user" name="user"
                       placeholder="Enter email">
                <br>
            </div>
            <div class="form-group">
                <label for="pwd">Password:</label>
                <input type="password" class="form-control" id="pwd" name="pwd" placeholder="Password">
            </div>
            <br>
            <button type="button" class="btn btn-success btn-lg active">Submit</button>
            <!--<button class="btn btn-success btn-lg active" type="submit">SAVE</button>-->
        </form>
    </div>
</div>
</body>
</html>
