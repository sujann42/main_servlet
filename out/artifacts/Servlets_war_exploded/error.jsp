<%--
  Created by IntelliJ IDEA.
  User: suzan
  Date: 2019-02-11
  Time: 21:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Error Page</title>
    <style>
        body{
            font-size: x-large; color:red;
        }
    </style>
</head>
<body>
Sorry, <%= session.getAttribute("errorMessage")%> <br>
This is an error page.
</body>
</html>
