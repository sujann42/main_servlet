package com.login;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = "Login")
public class Login extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {


        String username = request.getParameter("user");
        String password = request.getParameter("pwd");

        HttpSession session = request.getSession(true);

        if (username.equalsIgnoreCase("soozan@gmail.com") && password.equalsIgnoreCase("root")) {
            session.setAttribute("uname", username);
            response.sendRedirect("Success.jsp");
        } else {
           session.setAttribute("errorMessage", "Invalid Login Details");
            response.sendRedirect("error.jsp");
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {


    }
}
